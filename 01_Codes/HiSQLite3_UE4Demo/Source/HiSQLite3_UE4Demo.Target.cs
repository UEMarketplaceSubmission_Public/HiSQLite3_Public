/*
**	Copyright (c) 2016-2018 YeHaike(841660657@qq.com).
**	All rights reserved.
**	@ Date : 2017/11/21
*/

using UnrealBuildTool;
using System.Collections.Generic;

public class HiSQLite3_UE4DemoTarget : TargetRules
{
	public HiSQLite3_UE4DemoTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "HiSQLite3_UE4Demo" } );
	}
}
