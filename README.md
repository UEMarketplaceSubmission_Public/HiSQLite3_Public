# HiSQLite3 (SQLite3 for UE4)
## 
注意：针对Windows, 目前需要将对应x86或x64版本的sqlite3.dll拷贝到应用程序执行文件目录中。

## Blueprint Functions in "HiSQLite3" plugin

### SQLiteConnector Functions

<div align=center>
<img width="550" 
src="./README.md.resources/01_Images/000_CreateSQLiteConnector Functions.png"
/>
</div>


#### CreateSQLiteConnector

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/001_CreateSQLiteConnector.png"
/>
</div>

First of all. We should create a SQLiteConnector object with BP Function "CreateSQLiteConnector".

#### Open

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/002_Open.png"
/>
</div>



#### Execute(No RecordSet)

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/003_Execute(No RecordSet).png"
/>
</div>

#### Execute(With RecordSet)

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/004_Execute(With RecordSet).png"
/>
</div>


#### Close

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/005_Close.png"
/>
</div>

#### GetLastErrorInfo

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/006_GetLastErrorInfo.png"
/>
</div>

### SQLiteResultor Functions

<div align=center>
<img width="500" 
src="./README.md.resources/01_Images/007_SQLiteResultor Functions.png"
/>
</div>

#### GetColumnInfos

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/008_GetColumnInfos.png"
/>
</div>

#### GetRecordCount

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/009_GetRecordCount.png"
/>
</div>

#### IsAtEnd

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/010_IsAtEnd.png"
/>
</div>

#### MoveToFirst

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/011_MoveToFirst.png"
/>
</div>

#### MoveToNext

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/012_MoveToNext.png"
/>
</div>

#### GetString

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/013_GetString.png"
/>
</div>

#### GetInt

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/014_GetInt.png"
/>
</div>

#### GetBigInt

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/015_GetBigInt.png"
/>
</div>

#### GetFloat

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/016_GetFloat.png"
/>
</div>

#### SQLiteResultorToTable

<div align=center>
<img width="400" 
src="./README.md.resources/01_Images/017_SQLiteResultorToTable.png"
/>
</div>

### SQLite3 Database File Processing Functions

<div align=center>
<img width="600" 
src="./README.md.resources/01_Images/018_SQLite3 Database File Processing Functions.png"
/>
</div>

#### CopyFile

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/019_CopyFile.png"
/>
</div>

#### FileExists

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/020_FileExists.png"
/>
</div>

#### MoveFile

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/021_MoveFile.png"
/>
</div>

#### CopyFileForIOS

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/022_CopyFileForIOS.png"
/>
</div>

#### FileExistsForIOS

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/023_FileExistsForIOS.png"
/>
</div>

#### MoveFileForIOS

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/024_MoveFileForIOS.png"
/>
</div>

#### DeleteFile

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/025_DeleteFile.png"
/>
</div>

### SQLite3 Database File Path Processing Functions

<div align=center>
<img width="600" 
src="./README.md.resources/01_Images/026_SQLite3 Database File Path Processing Functions.png"
/>
</div>

#### PlatformIsIOS

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/027_PlatformIsIOS.png"
/>
</div>

#### CreateSQLiteConnector

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/028_CreateSQLiteConnector.png"
/>
</div>


#### GetGameName

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/029_GetGameName.png"
/>
</div>

#### GameContentDir

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/030_GameContentDir.png"
/>
</div>

#### GameDir

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/031_GameDir.png"
/>
</div>

#### GetAppResourcePathForIOS

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/032_GetAppResourcePathForIOS.png"
/>
</div>

#### Combine

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/033_Combine.png"
/>
</div>

#### ConvertRelativePathToFull

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/034_ConvertRelativePathToFull.png"
/>
</div>

#### GameContentDirForIOS

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/035_GameContentDirForIOS.png"
/>
</div>

#### DirectoryExists

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/036_DirectoryExists.png"
/>
</div>

#### GetAppDocumentPathForIOS

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/037_GetAppDocumentPathForIOS.png"
/>
</div>

#### GetAppPathForIOS

<div align=center>
<img width="200" 
src="./README.md.resources/01_Images/038_GetAppPathForIOS.png"
/>
</div>